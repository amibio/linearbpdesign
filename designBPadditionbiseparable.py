# LinearBPDesign
# Copyright (C) 2024 THEO BOURY 

import random  
from math import exp
from designBPbiseparable import is_leaf, children, deltaA, deltaC, children_colors_from_parent, get_assignments, get_assignments_unproper

def split(k, i):
    if i == 0:
        return [[]]
    if i == 1:
        return [[k]]
    else:
        res = []
        for l in range(k + 1):
            res += [ [l]+ ll for ll in split(k-l, i - 1)]
        return res
    

    
def HelixLastNode(v):
    if v[0] != "root":
        if len(v[1]) > 1: #it means that there are at least 2 BPs children or at least one leaf.
            return True
    return False

def num_design(v, c, current_level_modA, mA, leaves_levels_modA, current_level_modC, mC, leaves_levels_modC, cache, GCweight=None, k=0, h= 0, H=1):
    state = (v[0], c, current_level_modA,mA,leaves_levels_modA, current_level_modC,mC,leaves_levels_modC, k, h, H)
    if state not in cache:
        if is_leaf(v):
            if (c == "A") and (current_level_modA in leaves_levels_modA):
                #if (current_level_modA in leaves_levels_modA) and (current_level_modC in leaves_levels_modC):
                cache[state] = 1
            elif (c == "C") and (current_level_modC in leaves_levels_modC): #or (current_level_modC in leaves_levels_modC):
                cache[state] = 1
            else:
                cache[state] = 0
        elif HelixLastNode(v) and (k > 0) and (h < H):
            acc = num_design(v, c, current_level_modA,mA,leaves_levels_modA, current_level_modC,mC,leaves_levels_modC, cache, GCweight=GCweight, k=k, h=H, H=H)
            next_level_modA = (current_level_modA + deltaA(c)) % mA
            next_level_modC = (current_level_modC + deltaC(c)) % mC
            false_children = [((-5, -6),[])]
            for assignment in get_assignments(false_children,c):
                acc += num_design(v, assignment[0], next_level_modA, mA,leaves_levels_modA,next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight, k=k - 1, h=h+1, H=H)
            if GCweight is not None and (c in ["GC", "CG"]):
                cache[state] = exp(GCweight) * acc
            else:
                cache[state] = acc                
        elif (c=="AU" or c=="UA") and (current_level_modA in leaves_levels_modA):
            cache[state] = 0
        elif (c=="GC" or c=="CG") and (current_level_modC in leaves_levels_modC):
            cache[state] = 0
        else:
            acc = 0
            next_level_modA = (current_level_modA + deltaA(c)) % mA
            next_level_modC = (current_level_modC + deltaC(c)) % mC
            BPchildrenlen = len([vv for vv in children(v) if not is_leaf(vv)])
            for assignment in get_assignments(children(v),c):
                for loc_split in split(k, BPchildrenlen):
                    prod = 1
                    for i,w in enumerate(children(v)):
                        if assignment[i] == "A" or assignment[i] == "C":
                            k_loc = 0
                        else:
                            k_loc = loc_split.pop()
                        prod *= num_design(w, assignment[i], next_level_modA, mA,leaves_levels_modA,next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight, k=k_loc, h=0, H=H)
                    acc += prod
            if GCweight is not None and (c in ["GC", "CG"]):
                cache[state] = exp(GCweight) * acc
            else:
                cache[state] = acc
    return cache[state]

def next(c):
    return c
    #if c == "A":
    #    return "B"
    #if c == "U":
    #    return "V"
    #if c == "G":
    #    return "H"
    #if c == "C":
    #    return "D"
    #raise("Not a valid nucleotide")

def stochastic_backtrack(v, c, current_level_modA, mA, leaves_levels_modA,current_level_modC, mC, leaves_levels_modC, cache, GCweight=None, k=0, h=0, H=1):
    state = (v[0], c, current_level_modA, mA, leaves_levels_modA,current_level_modC, mC, leaves_levels_modC, k, h, H)
    if is_leaf(v):
        return (c, ".")
    elif HelixLastNode(v) and (k > 0) and (h < H):
        r = random.random()*cache[state]
        r -= num_design(v, c, current_level_modA, mA, leaves_levels_modA, current_level_modC, mC, leaves_levels_modC, cache, GCweight=GCweight, k=k, h=H, H=H)
        if r<0:
            res, struct_res = stochastic_backtrack(v, c, current_level_modA, mA, leaves_levels_modA, current_level_modC, mC, leaves_levels_modC, cache, GCweight=GCweight, k=k, h=H, H=H)
            if res is None:
                return None
            return (res, struct_res)
        next_level_modA = (current_level_modA + deltaA(c)) % mA
        next_level_modC = (current_level_modC + deltaC(c)) % mC
        false_children = [((-5, -6),[])]
        for assignment in get_assignments(false_children,c):
            if GCweight is not None and (c in ["GC", "CG"]):
                r-= exp(GCweight) *num_design(v, assignment[0], next_level_modA, mA,leaves_levels_modA, next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight, k=k - 1, h=h+1, H=H)
            else:
                r-= num_design(v, assignment[0], next_level_modA, mA,leaves_levels_modA, next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight, k=k - 1, h=h+1, H=H)
            if r<0:
                res, struct_res = stochastic_backtrack(v, assignment[0], next_level_modA, mA, leaves_levels_modA, next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight, k=k - 1, h=h+1, H=H)
                if res is None:
                    return None
                return (next(c[0]) + res + next(c[1]), "[" + struct_res + "]")
    else:
        acc = 0
        next_level_modA = (current_level_modA + deltaA(c)) % mA
        next_level_modC = (current_level_modC + deltaC(c)) % mC
        r = random.random()*cache[state]
        BPchildrenlen = len([vv for vv in children(v) if not is_leaf(vv)])
        for assignment in get_assignments(children(v),c):
            for loc_split in split(k, BPchildrenlen):
                prod = 1
                k_loc_list = []
                if GCweight is not None and (c in ["GC", "CG"]):
                    prod = exp(GCweight)
                for i,w in enumerate(children(v)):
                    if assignment[i] == "A" or assignment[i] == "C":
                        k_loc = 0
                    else:
                        k_loc = loc_split.pop()
                    k_loc_list.append(k_loc)
                    prod *= num_design(w, assignment[i], next_level_modA, mA,leaves_levels_modA,next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight, k=k_loc, h=0, H=H)
                r -= prod
                if r<0:
                    res = ""
                    struct_res = ""
                    for i,w in enumerate(children(v)):
                        loc, struct_loc = stochastic_backtrack(w, assignment[i], next_level_modA, mA,leaves_levels_modA, next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight, k=k_loc_list[i], h=0, H=H)
                        if loc is None:
                            return None
                        res += loc
                        struct_res += struct_loc
                    if c == "R":
                        return res, struct_res
                    else:
                        return (c[0]+res+c[1], "(" + struct_res + ")")


def sample_uniform_designs_mod2(t,k, GCweight=None, k2=0, h=0, H=1):
    def sample_single(acc,cache):
        r = random.random()*acc
        for leaves_levels_modA in [[], [0], [1]]:
            for leaves_levels_modC in [[], [0], [1]]:
                r -= num_design(t, "R", 0, 2, tuple(leaves_levels_modA),0, 2, tuple(leaves_levels_modC), cache, GCweight=GCweight, k=k2, h=h, H=H)
            if r<0:
                return stochastic_backtrack(t, "R", 0, 2, tuple(leaves_levels_modA),0, 2, tuple(leaves_levels_modC), cache, GCweight=GCweight, k=k2, h=h, H=H)
    acc = 0
    cache = {}
    for leaves_levels_modA in [[], [0], [1]]:
        for leaves_levels_modC in [[], [0], [1]]:
            acc += num_design(t, "R", 0, 2, tuple(leaves_levels_modA),0, 2, tuple(leaves_levels_modC), cache, GCweight=GCweight, k=k2, h=h, H=H)
    #print(acc)
    res = []
    return [sample_single(acc,cache) for i in range(k)]




    
def num_design_unproper(v, c, current_level_modA, mA, leaves_levels_modA,current_level_modC, mC, leaves_levels_modC, cache, GCweight=None, k=0, h= 0, H=1):
    state = (v[0], c, current_level_modA, mA, leaves_levels_modA,current_level_modC, mC, leaves_levels_modC, k, h, H)
    if state not in cache:
        if is_leaf(v):
            if (c == "A") and (current_level_modA in leaves_levels_modA):
                #if (current_level_modA in leaves_levels_modA) and (current_level_modC in leaves_levels_modC):
                cache[state] = 1
            elif (c == "C") and (current_level_modC in leaves_levels_modC): #or (current_level_modC in leaves_levels_modC):
                cache[state] = 1
            else:
                cache[state] = 0
        elif HelixLastNode(v) and (k > 0) and (h < H):
            acc = num_design_unproper(v, c, current_level_modA, mA, leaves_levels_modA,current_level_modC, mC, leaves_levels_modC, cache, GCweight=GCweight, k=k, h=H, H=H)
            next_level_modA = (current_level_modA + deltaA(c)) % mA
            next_level_modC = (current_level_modC + deltaC(c)) % mC
            false_children = [((-5, -6),[])]
            for assignment in get_assignments_unproper(false_children,c):
                acc += num_design_unproper(v, assignment[0], next_level_modA, mA,leaves_levels_modA,next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight, k=k - 1, h=h+1, H=H)
            if GCweight is not None and (c in ["GC", "CG"]):
                cache[state] = exp(GCweight) * acc
            else:
                cache[state] = acc  
        elif (c=="AU" or c=="UA") and (current_level_modA in leaves_levels_modA):
            cache[state] = 0
        elif (c=="GC" or c=="CG") and (current_level_modC in leaves_levels_modC):
            cache[state] = 0
        else:
            acc = 0
            next_level_modA = (current_level_modA + deltaA(c)) % mA
            next_level_modC = (current_level_modC + deltaC(c)) % mC
            BPchildrenlen = len([vv for vv in children(v) if not is_leaf(vv)])
            for assignment in get_assignments_unproper(children(v),c):
                for loc_split in split(k, BPchildrenlen):
                    prod = 1
                    for i,w in enumerate(children(v)):
                        if assignment[i] == "A" or assignment[i] == "C":
                            k_loc = 0
                        else:
                            k_loc = loc_split.pop()
                        prod *= num_design_unproper(w, assignment[i], next_level_modA, mA,leaves_levels_modA,next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight, k=k_loc, h=0, H=H)
                    acc += prod
            if GCweight is not None and (c in ["GC", "CG"]):
                cache[state] = exp(GCweight) * acc
            else:
                cache[state] = acc  
    return cache[state]


def stochastic_backtrack_unproper(v, c, current_level_modA, mA, leaves_levels_modA,current_level_modC, mC, leaves_levels_modC, cache, GCweight=None, k=0, h=0, H=1):
    state = (v[0], c, current_level_modA, mA, leaves_levels_modA,current_level_modC, mC, leaves_levels_modC, k, h, H)
    if is_leaf(v):
        return (c, ".")
    elif HelixLastNode(v) and (k > 0) and (h < H):
        r = random.random()*cache[state]
        r -= num_design_unproper(v, c, current_level_modA, mA, leaves_levels_modA,current_level_modC, mC, leaves_levels_modC, cache, GCweight=GCweight, k=k, h=H, H=H)
        if r<0:
            res, struct_res = stochastic_backtrack_unproper(v, c, current_level_modA, mA, leaves_levels_modA,current_level_modC, mC, leaves_levels_modC, cache, GCweight=GCweight, k=k, h=H, H=H)
            if res is None:
                print("HOLA")
                return None
            return (res, struct_res)
        next_level_modA = (current_level_modA + deltaA(c)) % mA
        next_level_modC = (current_level_modC + deltaC(c)) % mC
        false_children = [((-5, -6),[])]
        for assignment in get_assignments_unproper(false_children,c):
            if GCweight is not None and (c in ["GC", "CG"]):
                r-= exp(GCweight) *num_design_unproper(v, assignment[0], next_level_modA, mA,leaves_levels_modA,next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight, k=k - 1, h=h+1, H=H)
            else:
                r-= num_design_unproper(v, assignment[0], next_level_modA, mA,leaves_levels_modA,next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight, k=k - 1, h=h+1, H=H)
            if r<0:
                res, struct_res = stochastic_backtrack_unproper(v, assignment[0], next_level_modA, mA, leaves_levels_modA, next_level_modC, mC, leaves_levels_modC, cache, GCweight=GCweight, k=k - 1, h=h+1, H=H)
                if res is None:
                    print("HOLA")
                    return None
                return (next(c[0]) + res + next(c[1]), "[" + struct_res + "]")
    else:
        acc = 0
        next_level_modA = (current_level_modA + deltaA(c)) % mA
        next_level_modC = (current_level_modC + deltaC(c)) % mC
        r = random.random()*cache[state]
        BPchildrenlen = len([vv for vv in children(v) if not is_leaf(vv)])
        for assignment in get_assignments_unproper(children(v),c):
            for loc_split in split(k, BPchildrenlen):
                prod = 1
                k_loc_list = []
                if GCweight is not None and (c in ["GC", "CG"]): #and (c in ["GC", "CG"]) added
                    prod = exp(GCweight)
                for i,w in enumerate(children(v)):
                    if assignment[i] == "A" or assignment[i] == "C":
                        k_loc = 0
                    else:
                        k_loc = loc_split.pop()
                    k_loc_list.append(k_loc)
                    prod *= num_design_unproper(w, assignment[i], next_level_modA, mA,leaves_levels_modA,next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight, k=k_loc, h=0, H=H)
                r -= prod
                if r<0:
                    res = ""
                    struct_res = ""
                    for i,w in enumerate(children(v)):
                        loc, struct_loc = stochastic_backtrack_unproper(w, assignment[i], next_level_modA, mA,leaves_levels_modA,next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight, k=k_loc_list[i], h=0, H=H)
                        if loc is None:
                            return None
                        res += loc
                        struct_res += struct_loc
                    if c == "R":
                        return res, struct_res
                    else:
                        return (c[0]+res+c[1], "(" + struct_res + ")")



def sample_uniform_designs_mod2_unproper(t,k, GCweight=None, k2=0, h=0, H=1):
    def sample_single(acc,cache):
        r = random.random()*acc
        for leaves_levels_modA in [[], [0], [1], [0, 1]]:
            for leaves_levels_modC in [[], [0], [1], [0, 1]]:
                r -= num_design_unproper(t, "R", 0, 2, tuple(leaves_levels_modA),0, 2, tuple(leaves_levels_modC), cache, GCweight=GCweight, k=k2, h=h, H=H)
            if r<0:
                return stochastic_backtrack_unproper(t, "R", 0, 2, tuple(leaves_levels_modA),0, 2, tuple(leaves_levels_modC), cache, GCweight=GCweight, k=k2, h=h, H=H)
    acc = 0
    cache = {}
    for leaves_levels_modA in [[], [0], [1], [0, 1]]:
        for leaves_levels_modC in [[], [0], [1], [0, 1]]:
            acc += num_design_unproper(t, "R", 0, 2, tuple(leaves_levels_modA),0, 2, tuple(leaves_levels_modC), cache, GCweight=GCweight, k=k2, h=h, H=H)
    #print(acc)
    res = []
    return [sample_single(acc,cache) for i in range(k)]
