# LinearBPDesign
Copyright (C) 2023 THEO BOURY
## LinearBPDesign Project

This project presents LinearBPDesign, a dynamic programming algorithm to produce negative designs (under certain conditions). This repository contains also the functions to evaluate the practicality of these designs. 


### Prerequisite and launch
#### Dependencies

The tool uses the following non-standard libraries:

    ViennaRNA-2.5.1 (see https://www.tbi.univie.ac.at/RNA/ for more details.)
    pickle (for graph imports). (natively installed for Python above 3.7 ?)


#### Files and repositories

The project is entirely available on the current archive.

The different files are:

    designBP.py: contains the dynamic programming functions and the tests.
    BenchmarkFoldAndEval.py: contains necessary wrappers around RNAeval and RNAsubopts from ViennaRNA to compute Turner energy and corresponding structure.
    folding.py, a file that folds a sequence in a base pair minimization energy model.

### Test functions

All these functions can be run in a dynamic interpreter (launching designBP.py):

	* dbn_to_tree transforms the well-parenthesized word dbn into a rooted tree.
	* num_design, the dynamic programming scheme that counts the number of designs (also available with no proper condition on the multiloop num_design_unproper)
		** cache stores the DP table
		** v, node where the tree starts  
		** c the prescribed color for v
		** current_level_mod the current modulo level at node v 
		** m, maximal modulo m 
		** leaves_levels_mod modular repartition (for the leaves)
		** GCweight, the weight to put on GC to control GC content
	* stochastic_backtrack, the stochastic backtrack to get a design given a table cache, parameters as the same as num_design (also available with no proper condition on the multiloop stochastic_backtrack_unproper)
	* sample_uniform_designs_mod2 returns a design using modular levels 0 and 1. It is necessarily a negative design if the size of helices is strictly above 2 (also available with no proper condition on the multiloop sample_uniform_designs_mod2_unproper)
		** t, node where the tree starts 
		** k, number of samples
		** GCweight, the weight to put on GC to control GC content
	* first_modulo_separable returns for each sample minimal modulo (if exists) to ge a m-separated coloring
		** nb_samples_structs, the number of sampled secondary structures
		** modulolimit, the maximal modulo considered to check m-separability
		** n, size of the sequence
		** theta, minimal distance in nucleotides between extremities of a base pair
		** min_helix, the minimal size for helices 
		** checkDesignable if we recheck designability afterward as a sanity check
	* create_pickle_mfe_seqs_by_struct creates all sequences and folds them. Next store each structure according to if they admit an inverse folding or not.
		**n, size of the sequence
		** name, the name of the file to store
	* separable_at_size_n checks if sequences obtained above are linked with separated colorings or not
	* build_stats_dist_to_turner_proper_unproper_random_noM3oM5, build_stats_dist_to_turner_unproper_random_justM3oM5, build_GCcontent_min_max_DPproper_noM3oM5 build 2-separable design chosen randomly and check their energy distance compared to what would be get in a Turner energy model. 
		** nb_samples_structs, the number of sampled secondary structures
		** nb_samples_seqs, the number of sampled sequences by structure
		** n, size of the sequence
		** theta, minimal distance in nucleotides between extremities of a base pair
		** min_helix, the minimal size for helices 
### Contributors

    Théo Boury
    Hua-Ting Yao

