# LinearBPDesign
# Copyright (C) 2024 THEO BOURY 

import os

def ssparse(seq):
    res = [-1 for c in seq]
    p = []
    for i,c in enumerate(seq):
        if c=='(':
            p.append(i)
        elif c== ')':
            j = p.pop()
            (res[i],res[j]) = (j,i)
    return res


def sstopairs(ss):
    if ss is None:
        return None
    res = []
    for i in range(len(ss)):
        if ss[i]>i:
            j = ss[i]
            res.append((i,j))
    return set(res)


def hamming_distance(ss1, ss2):
    resu = 0
    se1 =sstopairs(ssparse(ss1))
    se2 = sstopairs(ssparse(ss2))
    return len(se1^se2)


def eval_and_compare_to_turner(seq, target_struct, h = 3, pr = 0):
    """
    Input:
        A sequence seq that we designed
        A target_structure, secondary structure that we target 
        A maximal BP distance h
        A boolean pr if we want some info prints or not
    Output: 
        The delta of energy compared to the best Turner structure at BP distance more than h and the corresponding structure
    """
    with open("designedseq.fa", "w") as f:
        f.write(">sequence1\n")
        f.write(seq + "\n")
    os.system("RNAfold -p -d2 --noLP < designedseq.fa > designedseqandturner.out")
    with open("designedseqandturner.out", "r") as f:
        li = (f.readlines()[2].strip()).split(" ")
        Turner_struct = li[0]
        if len(li[1]) <= 1:
            if len(li[2]) <= 1:
                Turner_energy = float(li[3][:-1])
            else:
                Turner_energy = float(li[2][:-1])
        else:
            Turner_energy = float(li[1][1:-1])
    with open("designedseqandtarget.txt", "w") as f:
        f.write(seq + "\n") 
        f.write(target_struct + "\n")    
    os.system("RNAeval -v -d2 < designedseqandtarget.txt > energydesignedseqandtarget.txt") 
    with open("energydesignedseqandtarget.txt", "r") as f:
        li = (f.readlines()[-1].strip()).split(" ")
        if len(li[1]) <= 1:
            if len(li[2]) <= 1:
                Target_energy = float(li[3][:-1])
            else:
                Target_energy = float(li[2][:-1])
        else:
            Target_energy = float(li[1][1:-1])
    if pr:
        print("Target energy: ", Target_energy)
        print("Turner energy: ", Turner_energy)
        print("Target structure: ", target_struct)
        print("Turner structure: ", Turner_struct)

    if hamming_distance(target_struct, Turner_struct) >= h:#Target_energy - Turner_energy != 0:
        os.system("rm designedseq.fa")
        os.system("rm designedseqandturner.out")
        os.system("rm designedseqandtarget.txt")
        os.system("rm energydesignedseqandtarget.txt")
        return Turner_energy - Target_energy, Turner_struct
    else:
        with open("designedseq.txt", "w") as f:
            f.write(seq + "\n")   
        os.system("RNAsubopt -v -s -d2 -e 5 < designedseq.txt > subopts.txt")
        with open("subopts.txt", "r") as f:
            li = f.readlines()[1:]        
        dd = 5.0
        struct = ""
        for elem in li:
            elembis = elem.strip().split(" ")
            struct, energy = elembis[0], elembis[1]
            if energy == "":
                energy = elembis[2]
                if energy == "":
                    energy = elembis[3]
            if hamming_distance(target_struct, struct) >= h: #In order to avoid slight variations of the MFE
                dd = float(energy) - Target_energy 
                break
        if pr:
            print("Delta Energy: ", dd)
        
        os.system("rm designedseq.txt")
        os.system("rm subopts.txt")
        os.system("rm designedseq.fa")
        os.system("rm designedseqandturner.out")
        os.system("rm designedseqandtarget.txt")
        os.system("rm energydesignedseqandtarget.txt")
        return dd, struct 

