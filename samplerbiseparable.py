"""This script contains functions related to one singe structure input w.r.t. Turner energy model
"""
import random
import pickle

import designBPbiseparable as design

def min_modulo_seperable(ss, modulolimitA=6, modulolimitC=6):
    """Compute minimum modulo seperable for given structure

    Args:
        ss: input secondary structure
        modulolimit: upper bound of modulo to check
    """
    tree = design.dbn_to_tree(design.ssparse(ss))
    num_design = design.num_design if design.filter(tree) else design.num_design_unproper
    for k in range(4, modulolimitA + modulolimitC + 1):
        cache = {}
        res = False
        for mA in range(2, modulolimitA + 1):
            for mC in range(2, modulolimitC + 1):
                if k == mC + mA:
                    for leaf_levelA in design.part(mA - 1):
                        for leaf_levelC in design.part(mC - 1):
                 # At least one solution in this leaf_level, thus (mA, mC)-seperable
                # We don't break for loop since we want all solutions of (mA, mC)-seperable
                            if num_design(tree, "R", 0, mA, tuple(leaf_levelA), 0, mC, tuple(leaf_levelC), cache, GCweight=None) > 0:
                                res = True
                if res == True:
                    return (mA, mC), cache
    # Here, ss is not (mA, mC)-seperable for (mA, mC) up to their respective modulolimit
    return None, None


class Sampler:
    """Separable solutions sampler of given structure
    The function generates (mA,mC)-separable solution with modulos mA, mC varies from the smzller value such as mA+mC is separable to uptomoduloA,uptomoduloC if given

    Args:
        ss: target structure
        modulolimitA: upper bound of modulo to check for A
        modulolimitC: upper bound of modulo to check for C
        uptomoduloA: modulo to include in solution if min modulo is smaller
        uptomoduloC: modulo to include in solution if min modulo is smaller
    """
    def __init__(self, ss, modulolimitA=10, modulolimitC=10, uptomoduloA=None, uptomoduloC=None, precompute=True):
        self.target_ss = ss
        self.target_tree = design.dbn_to_tree(design.ssparse(ss))
        self.isProper = design.filter(self.target_tree)
        self.num_design = design.num_design if self.isProper else design.num_design_unproper
        self.backtrack = design.stochastic_backtrack if self.isProper else design.stochastic_backtrack_unproper
        self.cache = {}
        self.min_moduloA = None
        self.min_moduloC = None
        self.choices = []
        self.weights = []
        if precompute:
            # Find minimum modulo and store the cache
            (self.min_moduloA,self.min_moduloC), self.cache = min_modulo_seperable(ss, modulolimitA=modulolimitA, modulolimitC=modulolimitC)
            # Fill cache for modulo from min_modulo+1 to uptomodulo
            uptoA = self.min_moduloA if uptomoduloA is None else uptomoduloA
            uptoC = self.min_moduloC if uptomoduloC is None else uptomoduloC
            if (self.min_moduloA is not None) and (self.min_moduloC is not None):
                for mA in [self.min_moduloA] + list(range(self.min_moduloA+1, uptoA+1)):
                    for mC in [self.min_moduloC] + list(range(self.min_moduloC+1, uptoC+1)):
                        for leaf_levelA in design.part(mA - 1):
                            for leaf_levelC in design.part(mC - 1):
                                self.choices.append(((mA, tuple(leaf_levelA)), (mC, tuple(leaf_levelC))))
                                self.weights.append(self.num_design(self.target_tree, "R", 0, mA, tuple(leaf_levelA), 0, mC, tuple(leaf_levelC), self.cache, GCweight=None))


    def dump_cache(self, path):
        """Dump precomputed modulo cache in pickle

        Args:
            path: path to store
        """
        with open(path, 'wb') as f:
            pickle.dump(self.cache, f)


    def load_cache(self, path):
        """Load precomputed modulo cache in pickle

        Args:
            path: path to cache
        """
        with open(path, 'rb') as f:
            self.cache = pickle.load(f)
        # TODO: function to determine minimum modulo

    def solution_number(self, mA, mC):
        """Get total number of solutions
        """
        count = 0
        for leaf_levelA in design.part(mA - 1):
            for leaf_levelC in design.part(mC - 1):
                nb = self.num_design(self.target_tree, "R", 0, mA, tuple(leaf_levelA), 0, mC, tuple(leaf_levelC), self.cache, GCweight=None)
                count += nb
        return count

    def fill_modulo(self, mA, mC):
        """Fill cache for given modulo A and C
        Return total number of solutions
        """
        count = 0
        for leaf_levelA in design.part(mA - 1):
            for leaf_levelC in design.part(mC - 1):
                self.choices.append(((mA, tuple(leaf_levelA)), (mC, tuple(leaf_levelC))))
                nb = self.num_design(self.target_tree, "R", 0, mA, tuple(leaf_levelA), 0, mC, tuple(leaf_levelC), self.cache, GCweight=None)
                self.weights.append(nb)
                count += nb
        return count


        return m, m, nb_sol

    def auto_fill(self, fromModulo=None, minSol=100, maxLevel=5):
        """Auto fill cache upto having at least nb solutions
        Return max modulo and solution counts

        Start from minmodulo if fromModulo is None
        """
        nb_sol = self.solution_number(self.min_moduloA, self.min_moduloC)
        cur_mA = self.min_moduloA
        cur_mC = self.min_moduloC
        level = 0
        while nb_sol < minSol and level < maxLevel:
            level += 1
            if fromModulo is None:
                upto = min(self.min_moduloA, self.min_moduloC) + level
            else:
                upto = fromModulo + level
            count = 0
            self.choices = []
            self.weights = []

            for mA in [self.min_moduloA] + list(range(self.min_moduloA+1, upto+1)):
                for mC in [self.min_moduloC] + list(range(self.min_moduloC+1, upto+1)):
                    for leaf_levelA in design.part(mA - 1):
                        for leaf_levelC in design.part(mC - 1):
                            self.choices.append(((mA, tuple(leaf_levelA)), (mC, tuple(leaf_levelC))))
                            nb = self.num_design(self.target_tree, "R", 0, mA, tuple(leaf_levelA), 0, mC, tuple(leaf_levelC), self.cache, GCweight=None)
                            self.weights.append(nb)
                            count += nb
            nb_sol = count
            cur_mA = upto
            cur_mC = upto

        return cur_mA, cur_mC, nb_sol


    def sample(self, verbose=False):
        ((mA, leaf_levelA), (mC, leaf_levelC)) = random.choices(self.choices, weights=self.weights)[0]
        seq = self.backtrack(self.target_tree, "R", 0, mA, leaf_levelA, 0, mC, leaf_levelC, self.cache, GCweight=None)
        if verbose:
            return seq, mA, mC
        else:
            return seq

    def sample_choice(self, mA, leaf_levelA, mC, leaf_levelC,verbose=False):
        #m, leaf_level = random.choices(self.choices, weights=self.weights)[0]
        seq = self.backtrack(self.target_tree, "R", 0, mA, leaf_levelA,0, mC, leaf_levelC, self.cache, GCweight=None)
        if verbose:
            return seq, m
        else:
            return seq
        
    def real_solutions(self):
        resu = []
        for l,((mA, leaf_levelA), (mC, leaf_levelC)) in enumerate(self.choices):
            resu_loc = []
            while len(resu_loc) < self.weights[l]:
                loc = self.sample_choice(mA, leaf_levelA, mC, leaf_levelC)
                if loc not in resu_loc:
                    resu_loc.append(loc)
            resu = [i for i in resu] + [i for i in resu_loc if i not in resu]
        return len(resu), resu
    def real_solutions_choose_modulo(self,mA, mC):
        resu = []
        choices = []
        weights = []
        for leaf_levelA in design.part(mA - 1):
            for leaf_levelC in design.part(mC - 1):
                choices.append(((mA, tuple(leaf_levelA)), (mC, tuple(leaf_levelC))))
                nb = self.num_design(self.target_tree, "R", 0, mA, tuple(leaf_levelA), 0, mC, tuple(leaf_levelC), self.cache, GCweight=None)
                weights.append(nb)
        for l,((mA, leaf_levelA), (mC, leaf_levelC)) in enumerate(choices):
            resu_loc = []
            while len(resu_loc) < weights[l]:
                loc = self.sample_choice(mA, leaf_levelA, mC, leaf_levelC)
                if loc not in resu_loc:
                    resu_loc.append(loc)
            resu = [i for i in resu] + [i for i in resu_loc if i not in resu]
        return len(resu), resu