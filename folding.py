# LinearBPDesign
# Copyright (C) 2024 THEO BOURY 

#This file contains the function to launch folding algorithms. 
# #The energy model supported is unitary. Weighted can be added by modifying "Energy" function.
#Considered BPs can contain G-U/U-G or not.
#For now, theta (min between two extremities of BP) = 0 and is not reported in the function.

import varnaapi
import math
from random import seed, choice

def convert(seq, listS, BPconsidered="Nussinov", output_format = "png", name_file = "", show=False, full = 0):
    """Input: - A sequence seq of nucleotides {A, U, C, G} of size n
              - A list of list of BPs S over seq
              - A model of BP considered BPconsidered
              - output_format is the type of file that we want to save, specify None for no save.
              - name_file is the name of the output Varna file if specified.
              - show is set to False if we want to observe the output directly or not (interactively).
       Output: If it exists, returns the well parenthesized structure for S over seq"""
    n = len(seq)
    list_resu = []
    for S in listS:
        resu = ["." for i in range(n)]
        for (i,j) in S:
            if not isValid(seq, i , j, BPconsidered=BPconsidered):
                return None
            resu[i] = "("
            resu[j] = ")"
        resu= "".join(resu)
        if resu != "."*n:
            list_resu.append(resu)
    if S != []:
        S = choice(listS)
        v  = varnaapi.Structure(seq, S)
        if output_format:
            if name_file == "":
                name = "SoverSeq" + "." + output_format
            else:
                name = name_file + "." + output_format
            v.savefig(name)
        if show:
            v.show()
    if full:
        return list_resu
    return len(listS)


def isValid(seq, i, j, BPconsidered="Nussinov"):
    """Input: - A Sequence seq of letters in {A, U, C, G} of size n
              - A BP (i, j) with i < n and j < n
              - A model of BP considered BPconsidered
       Output: If the BP (i, j) is feasible over seq or not from the "letters" constraints"""
    if BPconsidered == "Nussinov":
        S = [("A", "U"), ("U", "A"), ("G", "C"), ("C", "G"), ("G", "U"), ("U", "G")]
    elif BPconsidered == "Watson":
        S = [("A", "U"), ("U", "A"), ("G", "C"), ("C", "G")]
    else:
        raise ValueError("Not a valid model for BPs")
    return ((seq[i], seq[j]) in S)


def Energy(seq, i, j, model="Unitary", BPconsidered="Nussinov"):
    """Input: - A Sequence seq of letters in {A, U, C, G} of size n
              - A BP (i, j) with i < n and j < n
              - A energy model to work with
              - A model of BP considered BPconsidered
       Output: The energy of BP (i,j) in the specified model, currently only unitary energetic contribution is supported"""
    if model == "Unitary":
        if isValid(seq, i, j, BPconsidered=BPconsidered):
            return -1
        return 0
    #Add models if wanted


def FillMatUnitary(seq, model="Unitary", BPconsidered="Nussinov"):
    """Input: - A sequence seq of nucleotides {A, U, C, G} of size n
              - A energy model to work with
              - A model of BP considered BPconsidered
       Output: The dynamic programming table for folding over seq in a unitary or weigthed model"""
    n = len(seq)
    M = [[0 for i in range(n)] for j in range(n)]
           
    for i in range(n - 1,-1, -1):
        for j in range(i + 1, n):
            
            #i + 1 base is unpaired
            case_unpaired = M[i + 1][j] 
            
            #(i,j) is a "pair". No need to distinguish cases depending if the pair is valid or not, not the case with stackings
            case_embrace = M[i + 1][j - 1] + Energy(seq, i, j, model="Unitary", BPconsidered="Nussinov") 
            
            #(i,k) forms a "pair" (valid or not) that split in a subinstance (i + 1, k - 1) and an exterior instance (k + 1, j).
            case_split = min(case_unpaired, case_embrace) 
            for k in range(i + 1, j):
                case_split = min(case_split, M[i + 1][k - 1] + M[k + 1][j] + Energy(seq, i, k))
            M[i][j] = case_split
            
    return M
     

def DeltaBackTrackUnitary(sigma, Slist, delta, M, seq, model="Unitary", BPconsidered="Nussinov"):
    """Input: - Set of regions being considered sigma
              - Partial secondary structure asssigned to this point Slist
              - delta, max distance from the optimal allowed
              - The dynamic programming table computed M
              - A sequence seq of nucleotides {A, U, C, G} of size n
              - A energy model to work with
              - A model of BP considered BPconsidered
       Output: All optimal secondary structures over seq at delta from the optimal"""
       
    if sigma == []:
        return [Slist]
    
    (i,j) = sigma.pop()
    if j<= i:
        return DeltaBackTrackUnitary(sigma, Slist, delta, M, seq, model=model, BPconsidered=BPconsidered)
    
    resu = []
    
    delt= M[i + 1][j] - M[i][j]
    if delta - delt>= 0:
        newsigma = [(i + 1, j)] + sigma
        resu += DeltaBackTrackUnitary(newsigma, Slist, delta - delt, M, seq, model=model, BPconsidered=BPconsidered)
    
    delt= M[i + 1][j - 1] + Energy(seq, i, j, model=model, BPconsidered=BPconsidered) - M[i][j]
    if delta - delt>= 0 and isValid(seq, i, j, BPconsidered=BPconsidered):
        newsigma = [(i + 1, j - 1)] + sigma
        newSlist = [(i, j)] + Slist
        resu += DeltaBackTrackUnitary(newsigma, newSlist, delta - delt, M, seq, model=model, BPconsidered=BPconsidered)
    

    for k in range(i + 1, j):
        delt= M[i + 1][k -1] + M[k + 1][j] + Energy(seq, i, k, model=model, BPconsidered=BPconsidered) - M[i][j]
        if delta - delt>= 0 and isValid(seq, i, k, BPconsidered=BPconsidered):
            newsigma = [(i + 1, k - 1), (k + 1, j)] + sigma
            newSlist = [(i, k)] + Slist
            resu += DeltaBackTrackUnitary(newsigma, newSlist, delta - delt, M, seq, model=model, BPconsidered=BPconsidered)
    return resu


def delta_main_unitary(seq, model="Unitary", BPconsidered="Nussinov", delta = 0, debug = 0, output_format = "png", name_file = "", show=False, full = 0):
    """Input: - A Sequence seq of letters in {A, U, C, G} of size n
             - A energy model to work with
             - A model of BP considered BPconsidered
             - delta, max distance from the optimal allowed
             - debug, print for debug
             - output_format is the type of file that we want to save, specify None for no save.
             - name_file is the name of the output Varna file if specified.
             - show is set to False if we want to observe the output directly or not (interactively).
      Output: Build a MFE structure, print it and draw it  in the Unitary model"""
    M = FillMatUnitary(seq, model=model, BPconsidered=BPconsidered)
    if debug:
        print(M)
    Slist = DeltaBackTrackUnitary([(0, len(seq) - 1)], [], delta, M, seq, model=model, BPconsidered=BPconsidered)
    for S in Slist:
        if debug:
            print(S)   
    return convert(seq, Slist, output_format=output_format, name_file=name_file, show=show, full = full)
