# LinearBPDesign
# Copyright (C) 2024 THEO BOURY 

import random  
import csv
import numpy as np
import seaborn as sns
from math import exp
import matplotlib.pyplot as plt
import pickle
from itertools import product
from BenchmarkFoldAndEval import eval_and_compare_to_turner
from folding import delta_main_unitary
UNPAIRED_WEIGHT = 1


def ssparse(seq):
    res = [-1 for c in seq]
    p = []
    for i,c in enumerate(seq):
        if c=='(':
            p.append(i)
        elif c== ')':
            j = p.pop()
            (res[i],res[j]) = (j,i)
    return res


def sscount(n,count,count_stacked,theta,min_helix):
    if n not in count:
        if n<=0:
            count[n] = 1.
        else:
            count[n] = UNPAIRED_WEIGHT*sscount(n-1,count,count_stacked,theta,min_helix)
            if n >= theta+2*min_helix:
                count[n] += sscount_stacked(n-2*min_helix,count,count_stacked,theta,min_helix)
            for i in range(theta+2*min_helix,n):
                count[n] += sscount_stacked(i-2*min_helix,count,count_stacked,theta,min_helix)*sscount(n-i,count,count_stacked,theta,min_helix)
    return count[n]


def sscount_stacked(n,count,count_stacked,theta,min_helix):
    if n not in count_stacked:
        if n<=0:
            count_stacked[n] = 1.
        else:
            count_stacked[n] = UNPAIRED_WEIGHT*sscount(n-1,count,count_stacked,theta,min_helix)
            if n >= theta+2:
                count_stacked[n] += sscount_stacked(n-2,count,count_stacked,theta,min_helix)
            for i in range(theta+2*min_helix,n):
                count_stacked[n] += sscount_stacked(i-2*min_helix,count,count_stacked,theta,min_helix)*sscount(n-i,count,count_stacked,theta,min_helix)
    return count_stacked[n]


def ssrandom(n,count,count_stacked,theta,min_helix):
    if n<=0:
        return ''
    else:
        r = random.random()*sscount(n,count,count_stacked,theta,min_helix)
        r -= UNPAIRED_WEIGHT*sscount(n-1,count,count_stacked,theta,min_helix)
        if r<0:
            return '.'+ssrandom(n-1,count,count_stacked,theta,min_helix)
        if n >= theta+2*min_helix:
            r -= sscount_stacked(n-2*min_helix,count,count_stacked,theta,min_helix)
            if r<0:
                return '('*min_helix+ssrandom_stacked(n-2*min_helix,count,count_stacked,theta,min_helix)+')'*min_helix
        for i in range(theta+2*min_helix,n):
            r -= sscount_stacked(i-2*min_helix,count,count_stacked,theta,min_helix)*sscount(n-i,count,count_stacked,theta,min_helix)
            if r<0:
                return '('*min_helix+ssrandom_stacked(i-2*min_helix,count,count_stacked,theta,min_helix)+')'*min_helix+ssrandom(n-i,count,count_stacked,theta,min_helix)


def ssrandom_stacked(n,count,count_stacked,theta,min_helix):
    if n<=0:
        return ''
    else:
        r = random.random()*sscount_stacked(n,count,count_stacked,theta,min_helix)
        r -= UNPAIRED_WEIGHT*sscount(n-1,count,count_stacked,theta,min_helix)
        if r<0:
            return '.'+ssrandom(n-1,count,count_stacked,theta,min_helix)
        if n >= theta+2:
            r -= sscount_stacked(n-2,count,count_stacked,theta,min_helix)
            if r<0:
                return '('+ssrandom_stacked(n-2,count,count_stacked,theta,min_helix)+')'
        for i in range(theta+2*min_helix,n):
            r -= sscount_stacked(i-2*min_helix,count,count_stacked,theta,min_helix)*sscount(n-i,count,count_stacked,theta,min_helix)
            if r<0:
                return '('*min_helix+ssrandom_stacked(i-2*min_helix,count,count_stacked,theta,min_helix)+')'*min_helix+ssrandom(n-i,count,count_stacked,theta,min_helix)
            

def dbn_to_tree(dbn,i=None,j=None):
    if i is None:
        (i,j)=(0,len(dbn)-1)
        return ("root",dbn_to_tree(dbn,i,j))
    res = []
    while i<=j:
        if dbn[i] == -1:
            res.append(((i,i),[]))
            i+=1
        else:
            (ii,jj) = (i,dbn[i])
            res.append(((ii,jj),dbn_to_tree(dbn,ii+1,jj-1)))
            i = dbn[i]+1
    return res   


def is_leaf(v):
    """
    Input:
        a node v
    Output:
       Boolean if the node s a leaf or not 
    """
    if v[0] == "root":
        return False
    (a, b) = v[0]
    if a == b:
        return True 
    return False


def filter(v):
    BP_children = [vv[0] for vv in v[1] if not is_leaf(vv)]
    BP_leaves = [vv[0] for vv in v[1] if is_leaf(vv)]
    val = 3
    val2 = 1
    if v[0] == "root":
        val = 4
        val2 = 2
    if len(BP_children) > val or (len(BP_leaves) > 0 and len(BP_children) > val2):
        return False
    resu = True
    for vv in v[1]:
        resu = resu and filter(vv)
    return resu


def deltaA(c):
    if c=="GC":
        return 1
    elif c=="CG":
        return -1
    elif c=="AU":
        return 0
    elif c=="UA":
        return 0
    elif c == "R":
        return 0
    raise("Kernel panic!")
    
def deltaC(c):
    if c=="GC":
        return 0
    elif c=="CG":
        return 0
    elif c=="AU":
        return -1
    elif c=="UA":
        return 1
    elif c == "R":
        return 0
    raise("Kernel panic!")
  
children_colors_from_parent = {
    'AU' : ['AU','GC','CG'],
    'UA' : ['UA','GC','CG'],
    'GC' : ['UA','GC','AU'],
    'CG' : ['UA','CG','AU'],
    'R' : ['GC', 'UA','CG','AU']
}


def get_assignments(l,c,i=0):
    if i >= len(l):
        return [[]]
    else:
        if c == "R":           
            ii = i
            if is_leaf(l[i]):
                ii = i + 1
                while ii < len(l):
                    if not is_leaf(l[ii]):
                        break
                    ii += 1
                #if ii != len(l):
                return [["A"]*(ii-i) + lp for lp in get_assignments(l,c,ii) if ('AU' not in lp) and ('UA' not in lp) and ('C' not in lp)] + [["C"]*(ii-i) + lp for lp in get_assignments(l,c,ii) if ('GC' not in lp) and ('CG' not in lp) and ('A' not in lp)]
        v = l[i]
        if is_leaf(v):
            resu = []
            if c not in ["GC", "CG"]:
                resu += [['C'] + lp for lp in get_assignments(l,c,i+1) if ('GC' not in lp) and ('CG' not in lp) and ('A' not in lp)]
            if c not in ["AU","UA"]:
                resu += [['A'] + lp for lp in get_assignments(l,c,i+1) if ('AU' not in lp) and ('UA' not in lp) and ('C' not in lp)]
            return resu
            #else:
            #    return []
        
        else:
            res = []
            for cv in children_colors_from_parent[c]:
                for a in get_assignments(l,c,i+1):
                    if cv not in a:
                        boo = 0
                        if (cv not in ["AU","UA"]) or ('A' not in a):
                            boo = 1
                        if (cv not in ["GC","CG"]) or ('C' not in a):
                            boo = 1
                        if boo:
                            res.append([cv] + a)
            return res


def children(v):
    return v[1]
    
    
def num_design(v, c, current_level_modA, mA, leaves_levels_modA, current_level_modC, mC, leaves_levels_modC, cache, GCweight=None):
    state = (v[0], c, current_level_modA,mA,leaves_levels_modA, current_level_modC,mC,leaves_levels_modC)
    if state not in cache:
        if is_leaf(v):
            if (c == "A") and (current_level_modA in leaves_levels_modA):
            #if (current_level_modA in leaves_levels_modA) and (current_level_modC in leaves_levels_modC):
                cache[state] = 1
            elif (c == "C") and (current_level_modC in leaves_levels_modC): #or (current_level_modC in leaves_levels_modC):
                cache[state] = 1
            else:
                cache[state] = 0
        elif (c=="AU" or c=="UA") and (current_level_modA in leaves_levels_modA):
            cache[state] = 0
        elif (c=="GC" or c=="CG") and (current_level_modC in leaves_levels_modC):
            cache[state] = 0
        else:
            acc = 0
            next_level_modA = (current_level_modA + deltaA(c)) % mA
            next_level_modC = (current_level_modC + deltaC(c)) % mC
            for assignment in get_assignments(children(v),c):
                prod = 1
                if GCweight is not None and (c in ["GC", "CG"]):
                    prod = exp(GCweight)
                for i,w in enumerate(children(v)):
                    prod *= num_design(w, assignment[i], next_level_modA, mA,leaves_levels_modA, next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
                acc += prod
            cache[state] = acc
    return cache[state]


def stochastic_backtrack(v, c, current_level_modA, mA, leaves_levels_modA, current_level_modC, mC, leaves_levels_modC, cache, GCweight=None):
    state = (v[0], c, current_level_modA, mA, leaves_levels_modA, current_level_modC, mC, leaves_levels_modC)
    if is_leaf(v):
        return c
    else:
        acc = 0
        next_level_modA = (current_level_modA + deltaA(c)) % mA
        next_level_modC = (current_level_modC + deltaC(c)) % mC
        r = random.random()*cache[state]
        for assignment in get_assignments(children(v),c):
            prod = 1
            if GCweight is not None and (c in ["GC", "CG"]):
                prod = exp(GCweight)
            for i,w in enumerate(children(v)):
                prod *= num_design(w, assignment[i], next_level_modA, mA,leaves_levels_modA,  next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
            r -= prod
            if r<0:
                res = ""
                for i,w in enumerate(children(v)):
                    res += stochastic_backtrack(w, assignment[i], next_level_modA, mA,leaves_levels_modA,  next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
                if c == "R":
                    return res
                else:
                    return c[0]+res+c[1]


def sample_uniform_designs_mod2(t,k, GCweight=None):
    def sample_single(acc,cache):
        r = random.random()*acc
        for leaves_levels_modA in [[], [0], [1]]:
            for leaves_levels_modC in [[], [0], [1]]:
                r -= num_design(t, "R", 0, 2, tuple(leaves_levels_modA),0, 2, tuple(leaves_levels_modC), cache, GCweight=GCweight)
                if r<0:
                    return stochastic_backtrack(t, "R", 0, 2, tuple(leaves_levels_modA),0, 2, tuple(leaves_levels_modC), cache, GCweight=GCweight) 
    acc = 0
    cache = {}
    for leaves_levels_modA in [[], [0], [1]]:
        for leaves_levels_modC in [[], [0], [1]]:
            acc += num_design(t, "R", 0, 2, tuple(leaves_levels_modA),0, 2, tuple(leaves_levels_modC), cache, GCweight=GCweight)
    print(acc)
    res = []
    return [sample_single(acc,cache) for i in range(k)]


def choose_random_seq(t, withA=True):
    """
    Input:
        A tree t
        withA a boolean to say if we place only A on the unpaired positions or not
    Output:
        A random sequence compatible with the tree t
    """
    def seq_from_tree(v):
        if is_leaf(v):
            if withA:
                return "A"
            else:
                return random.choice(["A", "G", "C", "U"])
        else:
            res = ""
            BP = random.choice(["AU","UA", "GC", "CG"])
            for w in children(v):
                res= res + seq_from_tree(w)
            if v[0] == "root":
                return res
            else:
                return BP[0] + res + BP[1]
    return seq_from_tree(t)


def count_helix(t):
    """
    Input:
        A tree t
    Output:
        A random sequence compatible with the tree tThe list of the length of the helices in t
    """
    def count_helix_rec(v, current):
        if is_leaf(v):
            return []
        else:
            add = current + 1
            BP_children = [vv[0] for vv in v[1] if not is_leaf(vv)]
            BP_leaves = [vv[0] for vv in v[1] if is_leaf(vv)]
            resu = []
            if len(BP_children) > 1 or len(BP_leaves) > 0:
                resu = [add]
                add = 0
            if v[0] == "root":
                resu = []
                add = 0 
            for w in children(v):
                resu+= count_helix_rec(w, add)
            return resu
    return count_helix_rec(t, 0)


def isSeparable(t, seq, minmodulo = -1):
    """
    Input:
        A tree t
        A sequence seq that corresponds to a proper coloring
    Output:
        A boolean that say if coloring associated with seq is separable
        This coloring is supposed to be proper
    """
    def leaf_to_A(v):
        if is_leaf(v):
            if seq[v[0][0]] != "A":
                return False
            else:
                return True
        else:
            resu = True
            for w in children(v):
               resu = resu and leaf_to_A(w) 
            return resu
    def grey_and_leaf_level(v, current):
        if is_leaf(v):
            return [("l", current)]
        else:
            resu = []
            next_level = current
            if v[0] != "root":
                c = seq[v[0][0]] + seq[v[0][1]]
                if c == "AU" or c=="UA":
                    resu = [("g", current)]
                elif c == "GC":
                    next_level = current + 1  
                elif c == "CG":
                    next_level = current - 1  
            for w in children(v):
               resu = resu + grey_and_leaf_level(w, next_level)    
            return resu
    if not leaf_to_A(t):
        if minmodulo == -1:
            return False
        return False, -1
    li = grey_and_leaf_level(t, 0)
    LV_grey = [j for (i,j) in li if i == "g"]
    LV_leaf = [j for (i,j) in li if i == "l"]
    inter = [j for j in LV_leaf if j in LV_grey]
    m = -1
    for i in range(2, minmodulo + 1):
        LV_grey_mod = [j%i for j in LV_grey]
        LV_leaf_mod = [j%i for j in LV_leaf]
        inter_mod = [j for j in LV_leaf_mod if j in LV_grey_mod]
        if (inter_mod == []):
            m = i
            break
    if minmodulo == -1:
        return (inter == [])
    else:
        return (inter == []), m
    
    
def subset(li1, li2):
    sli1 = set(li1)
    bisli1 = list(sli1)
    bisli1.sort()
    if li1 == bisli1 and sli1.issubset(set(li2)):
        return True
    return False


def isProper(v, seq):
    """
    Input:
        A tree v 
        A sequence seq
    Output:
        A boolean that say if coloring associated with seq is proper
    """
    if is_leaf(v):
        return (seq[v[0][0]] == "A")
    else:
        if v[0] == "root":
            tolerated_colors = children_colors_from_parent['R']
        else:
            c = seq[v[0][0]] + seq[v[0][1]]
            if c not in ["AU", "UA", "GC", "CG"]:
                return False
            tolerated_colors = children_colors_from_parent[c]
        BP_children = [vv[0] for vv in v[1] if not is_leaf(vv)]
        colors_BP = [seq[name[0]] + seq[name[1]] for name in BP_children]
        colors_BP.sort()
        if not subset(colors_BP, tolerated_colors):
            return False
        else:
            resu = True
            for w in children(v):
                resu = resu and isProper(w, seq)
            return resu


def part(i):
    """
    Input:
        An interger i
    Output:
        A list of all distnct sets of i + 1 elements (as lists)
    """
    if i == -1:
        return [[]]
    else:
        li = part(i - 1)
        return li + [elem+[i] for elem in li]


def first_modulo_separable(nb_samples_structs=1000, modulolimit=10, n = 30, theta = 3, min_helix = 3, checkDesignable = 0):
    list_of_success = []
    list_of_failures = []
    list_of_failures_design = []
    stats = {}
    for i in range(nb_samples_structs):
        count,count_stacked = {},{}
        bo = False
        while bo == False:
            ss = ssrandom(n,count,count_stacked,theta,min_helix)
            t = dbn_to_tree(ssparse(ss))
            bo = filter(t)
        for m in range(2, modulolimit + 1):
            res = False
            for leaf_level in part(m - 1):
                cache = {}
                if num_design(t, "R", 0, m, tuple(leaf_level), cache, GCweight=None) > 0:
                    res = True
                    break
            if res == True:
                list_of_success.append((ss, m))
                stats[m] = stats.get(m, 0) + 1
                if checkDesignable:
                    seq = stochastic_backtrack(t, "R", 0, m, tuple(leaf_level), cache)
                    l = delta_main_unitary(seq, model="Unitary", BPconsidered="Nussinov", delta = 0, output_format = None, full = True)    
                    if len(l) > 1 or l[0] != ss:
                        print("Not designable", seq, ss, l[0])
                        list_of_failures_design.append((seq, ss, l[0]))
                break
            if m == modulolimit and res == False:
                list_of_failures.append(ss)
    print("Success by modulo", stats)
    print("Failed secondary structure with max modulo", modulolimit, ": ", list_of_failures)
    if checkDesignable:
        print("Failed secondary structure designable", list_of_failures_design)


def create_pickle_mfe_seqs_by_struct(n, name=None):
    """
    Input:
        size of the sequence n
        a name for the pickle file
    Output:
        Nothing, store in a pickle file named name or DictStructUniqueFold``n`` 
        the dictionnary of each secondary structure with its corresponding MFE sequences
    """
    li = [[]]
    for i in range(n):
        li_A = [elem + ["A"] for elem in li]
        li_C = [elem + ["C"] for elem in li]
        li_G = [elem + ["G"] for elem in li]
        li_U = [elem + ["U"] for elem in li]
        li = li_A + li_C + li_G + li_U 
    stats = {}
    counter = -1
    print("nb_iterations", len(li))
    for seq in li:
        counter+=1
        if counter %1000 == 0:
            print("Iteration", counter)
        seq2 = "".join(seq)
        l = delta_main_unitary(seq2, model="Unitary", BPconsidered="Nussinov", delta = 0, output_format = None, full = True)
        if len(l) == 1:
        #for ss in l: 
            ss = l[0]
            stats[ss] = stats.get(ss, []) + [seq2]
    pickle_name = "DictStructUniqueFold" + str(n)
    if name:
        pickle_name = name
    print(stats)
    with open(pickle_name, "wb") as f:
        pickle.dump(stats, f)


def separable_at_size_n(n, name=None):
    """
    Input:
        size of the sequences n
        a name for the pickle file, otherwise it is DictStructUniqueFold``n``
    Output:
        Returns the instances that are not (proper and separable).
    """
    counter = -1
    listS=[]
    if name:
        pickle_name = name
    else:
        pickle_name = "DictStructUniqueFold" + str(n)
    with open(pickle_name, "rb") as f:
        stats = pickle.load(f)
    print("nb_iterations", len(stats.keys()))
    for ss in stats.keys():
        counter+=1
        if counter %1000 == 0:
            print("Iteration", counter)
        t = dbn_to_tree(ssparse(ss))
        if filter(t):
            li_seq = stats[ss]
            proper_and_sep = False
            for seq in li_seq:
                if isSeparable(t, seq) and isProper(t, seq):
                    proper_and_sep = True
                    break
            if not proper_and_sep:
                print("Not proper or not separable", ss)
                listS.append(ss)
    return listS


def clear_stats():
    with open("DictStructUniqueFoldSeqs12", "rb") as f:
        stats = pickle.load(f)
    new_stats = {}
    new_new_stats = {}
    for ss in stats.keys():
        l = stats[ss]
        for seq in l:
            new_new_stats[seq] = new_new_stats.get(seq, []) + [ss]
    print(len(new_new_stats))
    for seq in new_new_stats.keys():
        if len(new_new_stats[seq]) == 1:
            new_stats[seq] = [new_new_stats[seq][0]]
    for seq in new_stats.keys():
        #print(seq)
        ss = new_stats[seq][0]
        #print(ss)
        t = dbn_to_tree(ssparse(ss))
        #print(t)
        (bo, m) = isSeparable(t, seq, minmodulo=10)
        #print(bo, m)
        new_stats[seq] = (ss, bo, m)
    print(len(new_stats))
    for elem in new_stats:
        print(elem, new_stats[elem])
    with open("StatsBySeqs12", "wb") as f:
        pickle.dump(new_stats, f)
    with open('StatsBySeqs12.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=' ',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(["Seq", "Secondary Structure", "isSeparable", "Minimal modulo"])
        for elem in new_stats.keys():
            (ss, bo, m) = new_stats[elem]
            writer.writerow([elem, ss, bo, m])


def get_assignments_unproper(l,c,i=0):
    if i >= len(l):
        return [[]]
    else:
        if c == "R":
            ii = i
            if is_leaf(l[i]):
                ii = i + 1
                while ii < len(l):
                    if not is_leaf(l[ii]):
                        break
                    ii += 1
                #if ii != len(l):
                return [["A"]*(ii-i) + lp for lp in get_assignments_unproper(l,c,ii) if ('AU' not in lp) and ('UA' not in lp) and ('C' not in lp)] + [["C"]*(ii-i) + lp for lp in get_assignments_unproper(l,c,ii) if ('GC' not in lp) and ('CG' not in lp) and ('A' not in lp)]
        v = l[i]
        if is_leaf(v):
            resu = []
            if c not in ["GC", "CG"]:
                resu += [['C'] + lp for lp in get_assignments_unproper(l,c,i+1) if ('GC' not in lp) and ('CG' not in lp) and ('A' not in lp)]
            if c not in ["AU","UA"]:
                resu += [['A'] + lp for lp in get_assignments_unproper(l,c,i+1) if ('AU' not in lp) and ('UA' not in lp) and ('C' not in lp)]
            return resu
        
        else:
            res = []
            for cv in ['GC', 'UA','CG','AU']:
                for a in get_assignments_unproper(l,c,i+1):
                    boo = 0
                    if (cv not in ["AU","UA"]) or ('A' not in a):
                        boo = 1
                    if (cv not in ["GC","CG"]) or ('C' not in a):
                        boo = 1
                    if boo:
                        res.append([cv] + a)
            return res

    
def num_design_unproper(v, c, current_level_modA, mA, leaves_levels_modA, current_level_modC, mC, leaves_levels_modC, cache, GCweight=None):
    state = (v[0], c, current_level_modA,mA,leaves_levels_modA, current_level_modC,mC,leaves_levels_modC)
    if state not in cache:
        if c == "??":
            #if is_leaf(v):
            #    cache[state] = num_design_unproper(v, "A", current_level_mod, m, leaves_levels_mod, cache, GCweight=GCweight)
            cache[state] = 0
            for couple in ["GC", "CG", "AU", "UA"]:
                prod = 1
                if GCweight is not None and (couple in ["GC", "CG"]): #and (c in ["GC", "CG"]) added
                    prod = exp(GCweight)
                cache[state] += prod*num_design_unproper(v, couple, current_level_modA, mA, leaves_levels_modA, current_level_modC, mC, leaves_levels_modC, cache, GCweight=GCweight)
            return cache[state]
        if is_leaf(v):
            if (c == "A") and (current_level_modA in leaves_levels_modA):
                #if (current_level_modA in leaves_levels_modA) and (current_level_modC in leaves_levels_modC):
                cache[state] = 1
            elif (c == "C") and (current_level_modC in leaves_levels_modC): #or (current_level_modC in leaves_levels_modC):
                cache[state] = 1
            else:
                cache[state] = 0
        elif (c=="AU" or c=="UA") and (current_level_modA in leaves_levels_modA):
            cache[state] = 0
        elif (c=="GC" or c=="CG") and (current_level_modC in leaves_levels_modC):
            cache[state] = 0
        else:
            acc = 0
            next_level_modA = (current_level_modA + deltaA(c)) % mA
            next_level_modC = (current_level_modC + deltaC(c)) % mC
            #for assignment in get_assignments_unproper(children(v),c):
            prod = 1
                #if GCweight is not None and (c in ["GC", "CG"]):
                #    prod = exp(GCweight)
            noleaves = 1
            for i,w in enumerate(children(v)):
                if is_leaf(w):
                    noleaves = 0
                    prod *= num_design_unproper(w, "A", next_level_modA, mA,leaves_levels_modA, next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
                else:
                    prod *= num_design_unproper(w, "??", next_level_modA, mA,leaves_levels_modA, next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
            acc += prod
            prod = 1
            if not noleaves:
                for i,w in enumerate(children(v)):
                    if is_leaf(w):
                        prod *= num_design_unproper(w, "C", next_level_modA, mA,leaves_levels_modA, next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
                    else:
                        prod *= num_design_unproper(w, "??", next_level_modA, mA,leaves_levels_modA, next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
            acc += prod
            cache[state] = acc
    return cache[state]


def stochastic_backtrack_unproper(v, c, current_level_modA, mA, leaves_levels_modA, current_level_modC, mC, leaves_levels_modC, cache, GCweight=None):
    state = (v[0], c, current_level_modA,mA,leaves_levels_modA, current_level_modC,mC,leaves_levels_modC)
    if c == "??":
        r = random.random()*cache[state]
        for couple in ["GC", "CG", "AU"]:
            prod = 1
            if GCweight is not None and (couple in ["GC", "CG"]): #and (c in ["GC", "CG"]) added
                prod = exp(GCweight)
            r-= prod*num_design_unproper(v, couple, current_level_modA, mA, leaves_levels_modA, current_level_modC, mC, leaves_levels_modC, cache, GCweight=GCweight)
            if r<0:
                return stochastic_backtrack_unproper(v, couple, current_level_modA, mA, leaves_levels_modA, current_level_modC, mC, leaves_levels_modC, cache, GCweight=GCweight)
        return stochastic_backtrack_unproper(v, "UA", current_level_modA, mA, leaves_levels_modA, current_level_modC, mC, leaves_levels_modC, cache, GCweight=GCweight)  
    if is_leaf(v):
        return c
    else:
        #acc = 0
        next_level_modA = (current_level_modA + deltaA(c)) % mA
        next_level_modC = (current_level_modC + deltaC(c)) % mC
        r = random.random()*cache[state]
        #for assignment in get_assignments_unproper(children(v),c):
        prod = 1
        noleaves = 1
                #if GCweight is not None and (c in ["GC", "CG"]):
                #    prod = exp(GCweight)
        for i,w in enumerate(children(v)):
            if is_leaf(w):
                noleaves = 0
                #print(prod)
                prod *= num_design_unproper(w, "A", next_level_modA, mA,leaves_levels_modA, next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
            else:
                #print(prod)
                prod *= num_design_unproper(w, "??", next_level_modA, mA,leaves_levels_modA, next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
 
        r -= prod
        if r < 0 or noleaves:
            res = ""
            for i,w in enumerate(children(v)):
                if is_leaf(w):
                    res += stochastic_backtrack_unproper(w, "A", next_level_modA, mA,leaves_levels_modA,  next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
                else:
                    res += stochastic_backtrack_unproper(w, "??", next_level_modA, mA,leaves_levels_modA,  next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
            if c == "R":
                return res
            else:
                return c[0]+res+c[1]
        prod = 1
        if not noleaves:
            #for i,w in enumerate(children(v)):
            #    if is_leaf(w):
            #        prod *= num_design_unproper(w, "C", next_level_modA, mA,leaves_levels_modA, next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
            #    else:
            #        prod *= num_design_unproper(w, "??", next_level_modA, mA,leaves_levels_modA, next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
            #r -= prod
            #if r < 0:
            res = ""
            for i,w in enumerate(children(v)):
                if is_leaf(w):
                    res += stochastic_backtrack_unproper(w, "C", next_level_modA, mA,leaves_levels_modA,  next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
                else:
                    res += stochastic_backtrack_unproper(w, "??", next_level_modA, mA,leaves_levels_modA,  next_level_modC, mC,leaves_levels_modC, cache, GCweight=GCweight)
            if c == "R":
                return res
            else:
                return c[0]+res+c[1]

def sample_uniform_designs_mod2_unproper(t,k, GCweight=None):
    def sample_single(acc,cache):
        r = random.random()*acc
        for leaves_levels_mod in [0, 1]:
            r -= num_design_unproper(t, "R", 0, 2, tuple([leaves_levels_mod]), cache, GCweight=GCweight)
            if r<0:
                return stochastic_backtrack_unproper(t, "R", 0, 2, tuple([leaves_levels_mod]), cache, GCweight=GCweight)
    acc = 0
    cache = {}
    for leaves_levels_mod in [0, 1]:
        acc += num_design_unproper(t, "R", 0, 2, tuple([leaves_levels_mod]), cache, GCweight=GCweight)
    print(acc)
    res = []
    return [sample_single(acc,cache) for i in range(k)]


def build_stats_dist_to_turner_proper_unproper_random_noM3oM5(nb_samples_structs=10000, nb_samples_seqs=1, n = 100, theta = 3, min_helix = 3):
    li = []
    for i in range(nb_samples_structs):
        count,count_stacked = {},{}
        bo = False
        while bo == False:
            ss = ssrandom(n,count,count_stacked,theta,min_helix)
            t = dbn_to_tree(ssparse(ss))
            bo = filter(t)
        l_unproper = sample_uniform_designs_mod2_unproper(t,nb_samples_seqs)
        l_random = [choose_random_seq(t, withA=True) for i in range(nb_samples_seqs)]
        l_proper = sample_uniform_designs_mod2(t,nb_samples_seqs)
        stats_unproper = {}
        stats_random = {}
        stats_proper = {}
        for elem in l_unproper:
            stats_unproper[elem] = stats_unproper.get(elem, 0) + 1
        for elem in l_random:
            stats_random[elem] = stats_random.get(elem, 0) + 1
        for elem in l_proper:
            stats_proper[elem] = stats_proper.get(elem, 0) + 1
        myline = [ss]
        for elem in stats_proper.keys():
            dd, struct = eval_and_compare_to_turner(elem,ss)
            myline.append(elem)
            myline.append(dd)
        for elem in stats_unproper.keys():
            dd, struct = eval_and_compare_to_turner(elem,ss)
            myline.append(elem)
            myline.append(dd)
        for elem in stats_random.keys():
            dd, struct = eval_and_compare_to_turner(elem,ss)
            myline.append(elem)
            myline.append(dd)
        li.append(myline.copy())
    with open('dist_to_turner_proper_unproper_random_noM3oM5n'+ str(n) + "theta" + str(theta) + "min_h" + str(min_helix) + "nb_samples" + str(nb_samples_structs) +'.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=' ',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(["Target Structure", "DP_Proper_Seq", "Dist_Turner_Proper", "DP_Unproper_Seq", "Dist_Turner_Unproper", "DP_Random_Seq", "Dist_Turner_Random"])
        for elem in li:
            writer.writerow(elem)
    sns.distplot([li[i][2] for i in range(len(li))], hist = True, kde = False,
                 kde_kws = {'linewidth': 1},
                 label = "DP sequence no proper condition")
    sns.distplot([li[i][6] for i in range(len(li))], hist = True, kde = False,
                 kde_kws = {'linewidth': 1},
                 label = "DP sequence proper coloring")
    plt.plot([0, 0], [0, 150])
    plt.legend(prop={'size': 10}, title = 'Sequence type')
    plt.title('Distance to Turner best secondary structure')
    plt.xlabel('Distance (kcal.mol-1)')
    plt.ylabel('Density')
    
def build_stats_dist_to_turner_unproper_random_justM3oM5(nb_samples_structs=10000, nb_samples_seqs=1, n = 100, theta = 3, min_helix = 3):
    li = []
    for i in range(nb_samples_structs):
        count,count_stacked = {},{}
        bo = True
        while bo == True:
            ss = ssrandom(n,count,count_stacked,theta,min_helix)
            t = dbn_to_tree(ssparse(ss))
            bo = filter(t)
        l_unproper = sample_uniform_designs_mod2_unproper(t,nb_samples_seqs)
        l_random = [choose_random_seq(t, withA=True) for i in range(nb_samples_seqs)]
        stats_unproper = {}
        stats_random = {}
        for elem in l_unproper:
            stats_unproper[elem] = stats_unproper.get(elem, 0) + 1
        for elem in l_random:
            stats_random[elem] = stats_random.get(elem, 0) + 1
        myline = [ss]
        for elem in stats_unproper.keys():
            dd, struct = eval_and_compare_to_turner(elem,ss)
            myline.append(elem)
            myline.append(dd)
        for elem in stats_random.keys():
            dd, struct = eval_and_compare_to_turner(elem,ss)
            myline.append(elem)
            myline.append(dd)
        li.append(myline.copy())
    with open('dist_to_turner_unproper_random_justM3oM5n'+ str(n) + "theta" + str(theta) + "min_h" + str(min_helix) + "nb_samples" + str(nb_samples_structs) +'.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=' ',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(["Target Structure", "DP_Unproper_Seq", "Dist_Turner_Unproper", "DP_Random_Seq", "Dist_Turner_Random"])
        for elem in li:
            writer.writerow(elem)
    sns.distplot([li[i][2] for i in range(len(li))], hist = True, kde = False,
                 kde_kws = {'linewidth': 1},
                 label = "DP sequence no proper condition")
    sns.distplot([li[i][4] for i in range(len(li))], hist = True, kde = False,
                 kde_kws = {'linewidth': 1},
                 label = "Random sequence")
    plt.plot([0, 0], [0, 150])
    plt.legend(prop={'size': 10}, title = 'Sequence type')
    plt.title('Distance to Turner best secondary structure')
    plt.xlabel('Distance (kcal.mol-1)')
    plt.ylabel('Density')
    
def build_GCcontent_min_max_DPproper_noM3oM5(nb_samples_structs=10000, nb_samples_seqs=100, n = 100, theta = 3, min_helix = 3):
    li = []
    for i in range(nb_samples_structs):
        count,count_stacked = {},{}
        bo = False
        while bo == False:
            ss = ssrandom(n,count,count_stacked,theta,min_helix)
            t = dbn_to_tree(ssparse(ss))
            bo = filter(t)
        mini = -35
        maxi = 31
        l_min = [None]
        while None in l_min:
            mini+=1
            l_min = sample_uniform_designs_mod2(t,nb_samples_seqs, GCweight=mini)
        l_max = [None]
        while None in l_max:
            maxi-=1
            l_max = sample_uniform_designs_mod2(t,nb_samples_seqs, GCweight=maxi)
        Gmincontent = n + 1
        Umincontent = n + 1
        Gmaxcontent = -1
        Umaxcontent = -1
        for elem in l_min:
            Gmincontent=min(Gmincontent, len([i for i in elem if i == "G"]))
            Umincontent=min(Umincontent, len([i for i in elem if i == "U"])) 
        #Gmincontent = Gmincontent/len(l_min)
        #Umincontent = Umincontent/len(l_min)
        for elem in l_max:
            Gmaxcontent=max(Gmaxcontent, len([i for i in elem if i == "G"]))
            Umaxcontent=max(Umaxcontent, len([i for i in elem if i == "U"]))
        #Gmaxcontent = Gmaxcontent/len(l_max)
        #Umaxcontent = Umaxcontent/len(l_max)
        myline = [ss]
        myline.append(mini)
        myline.append(maxi)
        myline.append(Gmincontent)
        myline.append(Umincontent)
        myline.append(Gmaxcontent)
        myline.append(Umaxcontent)
        li.append(myline.copy())
    with open('GCcontent_min_max_DPproper_noM3oM5n'+ str(n) + "theta" + str(theta) + "min_h" + str(min_helix) + "nb_samples" + str(nb_samples_structs) + "nb_seqs_by_struct" + str(nb_samples_seqs) +'.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=' ',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(["Target Structure", "MinWeight", "MaxWeight", "Mean#Gmin", "Mean#Umin", "Mean#Gmax", "Mean#Umax"])
        for elem in li:
            writer.writerow(elem)

import time
def first_modulo_separable_from_list(li, modulolimit=10, checkDesignable = 0):
    list_of_success = []
    list_of_failures = []
    list_of_failures_design = []
    stats = {}
    stats_unproper = {}
    unproper = 0
    for preindex, ss in enumerate(li):
        index = preindex + 1
        print(index)
        t = dbn_to_tree(ssparse(ss))
        proper = 1
        fun = num_design
        if filter(t) == False:
            proper = 0
            unproper +=1
            fun = num_design_unproper
        ttt = time.time()
        if index not in [28, 51, 68, 82]:
            for m in range(2, modulolimit + 1):
                res = False
                for leaf_level in part(m - 1):
                    cache = {}
                    if fun(t, "R", 0, m, tuple(leaf_level), cache, GCweight=None) > 0:
                        res = True
                        break
                if res == True:
                    print(index, "proper", proper, "success", m, "time", time.time() - ttt)
                    list_of_success.append((ss, m))
                    if proper:
                        stats[m] = stats.get(m, 0) + 1
                    else:
                        stats_unproper[m] = stats_unproper.get(m, 0) + 1
                    if checkDesignable:
                        seq = stochastic_backtrack(t, "R", 0, m, tuple(leaf_level), cache)
                        l = delta_main_unitary(seq, model="Unitary", BPconsidered="Nussinov", delta = 0, output_format = None, full = True)    
                        if len(l) > 1 or l[0] != ss:
                            print("Not designable", seq, ss, l[0])
                            list_of_failures_design.append((seq, ss, l[0]))
                    break
                if m == modulolimit and res == False:
                    list_of_failures.append(ss)
                print(index, "proper", proper, "failed with max modulo ", modulolimit, "time", time.time() -ttt)
    print("Unproper number", unproper)
    print("Success by modulo", stats)
    print("Success by modulo_unproper", stats_unproper)
    print("Failed secondary structure with max modulo", modulolimit, ": ", list_of_failures)
    if checkDesignable:
        print("Failed secondary structure designable", list_of_failures_design)
        
import csv


def csv_to_li(my_csv):
    with open(my_csv, "r") as f:
        li = [line.strip().split(',')[1] for line in f.readlines()]
    return li
