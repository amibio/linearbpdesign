# LinearBPDesign
# Copyright (C) 2024 THEO BOURY 

import random  
from math import exp
from designBP import is_leaf, children, delta,children_colors_from_parent, get_assignments, get_assignments_unproper, ssparse, dbn_to_tree

def build_prescription(seq, presc):
    resu = {}
    li = ssparse(seq)
    for i1 in range(len(presc)):
        pt1 = presc[i1]
        if pt1 != "N":
            i2 = li[i1] 
            if i2 == -1:
                resu[(i1, i1)] = pt1
            elif i1 < i2:
                resu[(i1, i2)] = pt1 + presc[i2]
    return resu

def num_design_unproper(v, c, current_level_mod, m, leaves_levels_mod, cache, GCweight=None, prescribed_multiloops={}):
    state = (v[0], c, current_level_mod,m,leaves_levels_mod)
    if state not in cache:
        if is_leaf(v):
            if current_level_mod in leaves_levels_mod:
                cache[state] = 1
            else:
                cache[state] = 0
        elif (c=="AU" or c=="UA") and (current_level_mod in leaves_levels_mod):
            cache[state] = 0
        else:
            acc = 0
            next_level_mod = (current_level_mod + delta(c)) % m
            boo = 1
            li_loc = []
            #print(children(v))
            for vv in children(v):
                if vv[0] not in prescribed_multiloops.keys():
                    boo = 0
                    break
                li_loc.append(prescribed_multiloops[vv[0]])
            
            if "A" in li_loc and "AU" in li_loc:
                print("Faulty assignment\n")
                boo = 0
            if "A" in li_loc and "UA" in li_loc:
                print("Faulty assignment\n")
                boo = 0 
            if "C" in li_loc or "G" in li_loc or "U" in li_loc:
                print("Faulty assignment\n")
                boo = 0
            if boo:
                li_assignments = [li_loc]
            else:
                li_assignments = get_assignments_unproper(children(v),c)
            for assignment in li_assignments:
                prod = 1
                if GCweight is not None and (c in ["GC", "CG"]): #and (c in ["GC", "CG"]) added
                    prod = exp(GCweight)
                for i,w in enumerate(children(v)):
                    prod *= num_design_unproper(w, assignment[i], next_level_mod, m,leaves_levels_mod, cache, GCweight=GCweight, prescribed_multiloops=prescribed_multiloops)
                acc += prod
            cache[state] = acc
    return cache[state]


def stochastic_backtrack_unproper(v, c, current_level_mod, m, leaves_levels_mod, cache, GCweight=None, prescribed_multiloops={}):
    state = (v[0], c, current_level_mod, m, leaves_levels_mod)
    if is_leaf(v):
        return c
    else:
        acc = 0
        next_level_mod = (current_level_mod + delta(c)) % m
        r = random.random()*cache[state]
        boo = 1
        li_loc = []
        for vv in children(v):
            #print(vv[0])
            if vv[0] not in prescribed_multiloops.keys():
                boo = 0
                break
            li_loc.append(prescribed_multiloops[vv[0]])
            
        if "A" in li_loc and "AU" in li_loc:
            print("Faulty assignment\n")
            boo = 0
        if "A" in li_loc and "UA" in li_loc:
            print("Faulty assignment\n")
            boo = 0 
        if "C" in li_loc or "G" in li_loc or "U" in li_loc:
            print("Faulty assignment\n")
            boo = 0
        #print(prescribed_multiloops.keys())
        #if boo:
        #    print(boo)
        if boo:
            li_assignments = [li_loc]
        else:
            li_assignments = get_assignments_unproper(children(v),c)
        for assignment in li_assignments:
            prod = 1
            if GCweight is not None and (c in ["GC", "CG"]): #and (c in ["GC", "CG"]) added
                prod = exp(GCweight)
            for i,w in enumerate(children(v)):
                prod *= num_design_unproper(w, assignment[i], next_level_mod, m,leaves_levels_mod, cache, GCweight=GCweight, prescribed_multiloops=prescribed_multiloops)
            r -= prod
            if r<0:
                res = ""
                for i,w in enumerate(children(v)):
                    res += stochastic_backtrack_unproper(w, assignment[i], next_level_mod, m,leaves_levels_mod, cache, GCweight=GCweight, prescribed_multiloops=prescribed_multiloops)
                if c == "R":
                    return res
                else:
                    return c[0]+res+c[1]


def sample_uniform_designs_mod2_unproper(t,k, GCweight=None, prescribed_multiloops={}):
    def sample_single(acc,cache):
        r = random.random()*acc
        for leaves_levels_mod in [0, 1]:
            r -= num_design_unproper(t, "R", 0, 2, tuple([leaves_levels_mod]), cache, GCweight=GCweight, prescribed_multiloops=prescribed_multiloops)
            if r<0:
                return stochastic_backtrack_unproper(t, "R", 0, 2, tuple([leaves_levels_mod]), cache, GCweight=GCweight, prescribed_multiloops=prescribed_multiloops)
    acc = 0
    cache = {}
    for leaves_levels_mod in [0, 1]:
        acc += num_design_unproper(t, "R", 0, 2, tuple([leaves_levels_mod]), cache, GCweight=GCweight, prescribed_multiloops=prescribed_multiloops)
    print(acc)
    res = []
    return [sample_single(acc,cache) for i in range(k)]

