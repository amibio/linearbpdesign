"""This script contains functions related to one singe structure input w.r.t. Turner energy model
"""
import random
import pickle

import designBP as design

def min_modulo_seperable(ss, modulolimit=10):
    """Compute minimum modulo seperable for given structure

    Args:
        ss: input secondary structure
        modulolimit: upper bound of modulo to check
    """
    tree = design.dbn_to_tree(design.ssparse(ss))
    num_design = design.num_design if design.filter(tree) else design.num_design_unproper
    for m in range(2, modulolimit + 1):
        cache = {}
        res = False
        for leaf_level in design.part(m - 1):
            # At least one solution in this leaf_level, thus m-seperable
            # We don't break for loop since we want all solutions of m-seperable
            if num_design(tree, "R", 0, m, tuple(leaf_level), cache, GCweight=None) > 0:
                res = True
        if res == True:
            return m, cache
    # Here, ss is not m-seperable for m up to modulolimit
    return None, None


class Sampler:
    """Separable solutions sampler of given structure
    The function generates m-separable solution with modulo m varies from structure minimum modulo separable to uptomodulo, if given

    Args:
        ss: target structure
        modulolimit: upper bound of modulo to check
        uptomodulo: modulo to include in solution if min modulo if smaller
    """
    def __init__(self, ss, modulolimit=10, uptomodulo=None, precompute=True):
        self.target_ss = ss
        self.target_tree = design.dbn_to_tree(design.ssparse(ss))
        self.isProper = design.filter(self.target_tree)
        self.num_design = design.num_design if self.isProper else design.num_design_unproper
        self.backtrack = design.stochastic_backtrack if self.isProper else design.stochastic_backtrack_unproper
        self.cache = {}
        self.min_modulo = None

        self.choices = []
        self.weights = []
        if precompute:
            # Find minimum modulo and store the cache
            self.min_modulo, self.cache = min_modulo_seperable(ss, modulolimit=modulolimit)
            # Fill cache for modulo from min_modulo+1 to uptomodulo
            upto = self.min_modulo if uptomodulo is None else uptomodulo
            if self.min_modulo is not None:
                for m in [self.min_modulo]+list(range(self.min_modulo+1, upto+1)):
                    for leaf_level in design.part(m - 1):
                        self.choices.append((m, tuple(leaf_level)))
                        self.weights.append(self.num_design(self.target_tree, "R", 0, m, tuple(leaf_level), self.cache, GCweight=None))


    def min_helix_length(self):
        """Get minimum helix length
        """
        lst = design.count_helix(self.target_tree)
        if len(lst) >= 1:
            return min(lst)
        return 0

    def solution_number(self, modulo):
        """Get solution numbers of given modulo
        """
        nb = 0
        for leaf_level in design.part(modulo-1):
            nb += self.num_design(self.target_tree, "R", 0, modulo, tuple(leaf_level), self.cache, GCweight=None)

        return nb


    def auto_fill(self, fromModulo=None, minSol=100, maxLevel=5):
        """Auto fill cache upto having at least nb solutions
        Return max modulo and solution counts
        """
        nb_sol = self.solution_number(self.min_modulo)
        cur_m = self.min_modulo
        level = 0
        while nb_sol < minSol and level < maxLevel:
            level += 1
            if fromModulo is None:
                upto = self.min_modulo + level
            else:
                upto = fromModulo + level
            count = 0
            self.choices = []
            self.weights = []
            for m in [self.min_modulo]+list(range(self.min_modulo+1, upto+1)):
                for leaf_level in design.part(m - 1):
                    self.choices.append((m, tuple(leaf_level)))
                    nb = self.num_design(self.target_tree, "R", 0, m, tuple(leaf_level), self.cache, GCweight=None)
                    self.weights.append(nb)
                    count += nb
            nb_sol = count
            cur_m = upto
        return cur_m, nb_sol


    def fill_modulo(self, modulo, modulolimit=10):
        """Fill cache for given modulo
        Run on minimum modulo if modulis is not given or less than 2 and stores min_modulo and cache

        Args:
            modulo: modulo to fill
        """
        if modulo is None or modulo < 2:
            self.min_modulo, self.cache = min_modulo_seperable(self.target_ss, modulolimit=modulolimit)
            modulo = self.min_modulo
        for leaf_level in design.part(modulo):
            self.choices.append((modulo, tuple(leaf_level)))
            self.weights.append(self.num_design(self.target_tree, "R", 0, modulo, tuple(leaf_level), self.cache, GCweight=None))


    def dump_cache(self, path):
        """Dump precomputed modulo cache in pickle

        Args:
            path: path to store
        """
        with open(path, 'wb') as f:
            pickle.dump(self.cache, f)


    def load_cache(self, path):
        """Load precomputed modulo cache in pickle

        Args:
            path: path to cache
        """
        with open(path, 'rb') as f:
            self.cache = pickle.load(f)
        # TODO: function to determine minimum modulo

    def sample(self, verbose=False):
        m, leaf_level = random.choices(self.choices, weights=self.weights)[0]
        seq = self.backtrack(self.target_tree, "R", 0, m, leaf_level, self.cache, GCweight=None)
        if verbose:
            return seq, m
        else:
            return seq

    def sample_choice(self, m, leaf_level, verbose=False):
        #m, leaf_level = random.choices(self.choices, weights=self.weights)[0]
        seq = self.backtrack(self.target_tree, "R", 0, m, leaf_level, self.cache, GCweight=None)
        if verbose:
            return seq, m
        else:
            return seq
        
    def real_solutions(self):
        resu = []
        for l,(m, leaf_level) in enumerate(self.choices):
            resu_loc = []
            while len(resu_loc) < self.weights[l]:
                loc = self.sample_choice(m, leaf_level)
                if loc not in resu_loc:
                    resu_loc.append(loc)
            resu = [i for i in resu] + [i for i in resu_loc if i not in resu]
        return len(resu), resu
    
    def real_solutions_choose_modulo(self, m):
        resu = []
        choices = []
        weights = []
        
        for leaf_level in design.part(m - 1):
            choices.append((m, tuple(leaf_level)))
            nb = self.num_design(self.target_tree, "R", 0, m, tuple(leaf_level), self.cache, GCweight=None)
            weights.append(nb)
        for l,(m, leaf_level) in enumerate(choices):
            resu_loc = []
            while len(resu_loc) < weights[l]:
                loc = self.sample_choice(m, leaf_level)
                if loc not in resu_loc:
                    resu_loc.append(loc)
            resu = [i for i in resu] + [i for i in resu_loc if i not in resu]
        return len(resu), resu